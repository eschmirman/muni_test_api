MUNI technical test :: Festivity api

Service used for add, edit, get and delete, festivities 

## **BASIC INSTALLATION**
- Install and configure PHP V >= 5.6
- Install and configure Apache V >= 2.2
- Install and configure ddbb (mysql V >= 5 or mariaDB V >= 2)
- Create a DDBB with api.sql file
- Put the application (source code) into the apache public folder
- Configure db connection into utils/db/ddbb.class.php file
- If necesary, run bulk import endpoint
- Thats all folks!

## **BULK IMPORT**
For import items from files, put XML files into "import" folder with this structure
`<?xml version="1.0" ?>
<festivities><br>
    <festivity>
        <name>Jimmy's event</name>
        <place>Clark's castle</place>
        <start>2020-02-22T19:16:01.001Z</start>
        <end>2020-07-30T19:16:01.001Z</end>
    </festivity>
    ...
</festivities>`

## **ENDPOINTS**

* ### POST /festivities/bulk_import
* ### POST /festivities
    {
        "name":"Gerald's amazing days cont 2010",
        "start_date":"2020-05-12T19:32:45.045Z",
        "end_date":"2020-08-22T19:32:45.045Z",
        "place":"Schmidt's joint 2010"
    }
* ### GET /festivities/{id}
* ### GET /festivities/search?start_date=&end_date=&place=&name=
* ### PUT /festivities/{id}
    {
        "name":"Gerald's amazing days cont 2010",
        "start_date":"2020-05-12T19:32:45.045Z",
        "end_date":"2020-08-22T19:32:45.045Z",
        "place":"Schmidt's joint 2010"
    }
* ### DELETE /festivities/{id}

## **ABOUT THE SOLUTION**
* index.php is the main controller (implemented with Epiphany library) for expone necessary endpoints to interact with the api
* an entity service process the necessary logic to save or retrieve entities; with minimal logic and all methods only execute own responsibility
* after process; service return a json with the required information (record or list) with http 2xx error code
* if occurs some error, there is implemented abstract Excpetion for return 4xx or 5xx error code with message body 
* logs can be found into logs folder; with this implementation we can configure (log4php) alternatives for logs files or events
* for ddbb logic used redBean library for save and get; with abstracted classes, any model from/to ddbb

## **PUBLIC API WITH EXAMPLES**
https://www.getpostman.com/collections/86975406e1e0c94cd94f

## **RUNNING TESTS**
From local CLI run: <br>
composer require --dev phpunit/phpunit ^9<br>
composer install (1st time only for install depencies)<br>
cd PROJECT_PATH && ./vendor/bin/phpunit tests<br>