<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('content-type: application/json; charset=utf-8');

require_once('utils/db/ddbb.class.php');
require_once('utils/log/log4php/Logger.php');
require_once('services/FestivityService.php');
require_once('services/BulkImportService.php');
require_once('epiphany/src/Epi.php');

Epi::setSetting('exceptions', true);
Epi::setPath('base', 'epiphany/src');
Epi::init('api');

$GLOBALS['db']=Database::getInstance()->getConnection();

Logger::configure('config/log4php.xml');
$GLOBALS['log']=Logger::getLogger("main");

//@TODO validate client using authorization header
/* BOM - controller endpoints */
getRoute()->post('/festivities', array('FestivityService', 'create'));
getRoute()->post('/festivities/bulk_import', array('BulkImportService', 'bulkImport'));

getRoute()->get('/festivities/(\d+)', array('FestivityService', 'get'));
getRoute()->get('/festivities/search', array('FestivityService', 'getList'));

getRoute()->put('/festivities/(\d+)', array('FestivityService', 'update'));

getRoute()->delete('/festivities/(\d+)', array('FestivityService', 'delete'));
/* EOM - controller endpoints */

getRoute()->run();

?>

