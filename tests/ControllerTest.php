<?php
use PHPUnit\Framework\TestCase;

class ControllerTest extends TestCase
{
    private $http;
    private $mockedFestivityObjectId;
    private $mockedFestivityObject = array(
        'name' => 'test festivity example',
        'start_date' => '2021-05-08t16:28:48.048z',
        'end_date' => '2021-05-10t19:32:48.048z',
        'place' => 'buenos aires'
    );

    protected function setUp(): void {
        $this->http = new GuzzleHttp\Client(['base_uri' => 'http://localhost/muni_test_api/']);
        $this->mockedFestivityObjectId = $this->createFestivityByPost($this->mockedFestivityObject);

        parent::setUp();
    }

    protected function tearDown(): void {
        parent::tearDown();
    }

    public function testGetFestivityByInvalidIdReturnHttp404() {
        $response = $this->http->request(
            'GET',
            'festivities/0',
            ['http_errors' => false]
        );
        $this->assertEquals("404", $response->getStatusCode());
    }

    public function testGetFestivityByIdReturnHttp200WithExpectedBody() {
        $response = $this->http->request(
            'GET',
            'festivities/'.$this->mockedFestivityObjectId
        );
        $festivityResponse = json_decode( $response->getBody());

        $this->assertEquals("200", $response->getStatusCode());
        $this->assertEquals($this->mockedFestivityObjectId, $festivityResponse->id);

        $this->assertEquals($this->mockedFestivityObject['name'], $festivityResponse->name);
        $this->assertEquals($this->mockedFestivityObject['start_date'], $festivityResponse->start_date);
        $this->assertEquals($this->mockedFestivityObject['end_date'], $festivityResponse->end_date);
        $this->assertEquals($this->mockedFestivityObject['place'], $festivityResponse->place);
    }

    public function testPutFestivityByIdReturnHttp200() {
        $festivity = $this->mockedFestivityObject;
        $festivity['name'] = 'new name';

        $response = $this->http->request(
            'PUT',
            'festivities/'.$this->mockedFestivityObjectId,
            ['http_errors' => false, 'json' => $festivity]
        );
        $festivityResponse = json_decode($response->getBody());

        $this->assertEquals("200", $response->getStatusCode());
        $this->assertEquals($this->mockedFestivityObjectId, $festivityResponse->id);
    }

    public function testPutFestivityByInvalidIdReturnHttp404() {
        $festivity = $this->mockedFestivityObject;
        $festivity['name'] = 'new name';

        $response = $this->http->request(
            'PUT',
            'festivities/0',
            ['http_errors' => false, 'json' => $festivity]
        );
        $this->assertEquals("404", $response->getStatusCode());
    }

    public function testPutFestivityByInvalidBodyReturnHttp400() {
        $response = $this->http->request(
            'PUT',
            'festivities/'.$this->mockedFestivityObjectId,
            ['http_errors' => false]
        );
        $this->assertEquals("400", $response->getStatusCode());
    }

    public function testDeleteFestivityByIdReturnHttp200() {
        $response = $this->http->request(
            'DELETE',
            'festivities/'.$this->mockedFestivityObjectId,
            ['http_errors' => false]
        );
        $this->assertEquals("200", $response->getStatusCode());
    }

    public function testDeleteFestivityByInvalidIdReturnHttp404() {
        $response = $this->http->request(
            'DELETE',
            'festivities/0',
            ['http_errors' => false]
        );
        $this->assertEquals("404", $response->getStatusCode());
    }

    private function createFestivityByPost($festivity) {
        $response = $this->http->request('POST',  'festivities', ['json' => $festivity]);
        $festivityResponse = json_decode($response->getBody());
        return $festivityResponse->id;
    }


}