<?php

class ExceptionService
{
    static function handleException($errorMsg, $errorCode) {
        $GLOBALS['log']->error($errorMsg);
        EpiException::raise(new EpiException($errorMsg, $errorCode));
    }

}

?>

