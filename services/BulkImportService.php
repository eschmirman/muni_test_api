<?php

require_once('services/ExceptionService.php');
require_once('services/FestivityService.php');

class BulkImportService
{
    private static $importFolderPath = './import/';
    private static $importFileExtension = 'xml';

    static public function bulkImport() {
        $processed_items = 0;
        $files = self::getFilesToProcess(self::$importFolderPath, self::$importFileExtension);

        foreach($files as $file) {
            $fullPathFile = self::$importFolderPath.$file;
            $items = self::getItemsFromFile($fullPathFile, self::$importFileExtension);

            foreach($items as $item) {
                FestivityService::save(array(
                        'name' => strtolower($item->name),
                        'place' => strtolower($item->place),
                        'start_date' => strtolower($item->start),
                        'end_date' => strtolower($item->end)
                    )
                );
            }
            $GLOBALS['log']->info("BulkImport: " . count($items) . " items created from file: $fullPathFile");
            self::backupProcessedFile($fullPathFile);
        }

        if(isset($items)) {
            $processed_items = count($items);
        }
        echo json_encode(array('processed_items' => $processed_items));
    }

    static public function getFilesToProcess($pathToScan, $searchExtension) {
        $files=array();

        if ($handle = opendir($pathToScan)) {

            while(false !== ($fileItem=readdir($handle))) {
                if($fileItem != "." && $fileItem != "..") {
                    $fileInfo = pathinfo($fileItem);
                    if($fileInfo['extension'] == $searchExtension) {
                        $files[]=$fileItem;
                    }
                }
            }

            closedir($handle);
        } else {
            ExceptionService::handleException("error listing files in " . self::$pathToScan, 500);
        }


        return $files;
    }

    static private function getItemsFromFile($filePath, $fileExtension) {
        //TODO validate file extension for distincts file extensions support
        $itemsFromSource=simplexml_load_file($filePath);
        return $itemsFromSource->festivity;
    }

    static private function backupProcessedFile($fullPathFile) {
        rename($fullPathFile, $fullPathFile.'.'.date("YmdHis"));
    }

}

?>

